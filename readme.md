A Plugin for controlling Cabletime Mediastar endpoints.

The plugin can control the endpoint using either serial or TCP. All functions are available. A direct channel value can be input using the "channel in" pin. Useful for preset channel values.
In the properties panel set connection mode, Ethernet or Serial.The IP address may be entered in the GUI or using the control pin. Password ( if used ) may also be set in the properties panel.

Control Pins are available for all functions. An indicator on the GUI shows the connection state.



    
